<?php

/*
* File Name: Protocol.php
* Author: Len
* mail: i@91coder.org
* Created Time: Sun 26 Mar 2017 02:50:37 AM CST
* 
*/

class Protocol
{

    public $swoole_config;
    public $redis_config;
    public $worker_num;
    public $log_config;
    public $time_rate;
    public $serv;

    public function __construct($config)
    {
        $this->initConfig($config);
        Log::ini_set($this->log_config);
    }

    //初始化配置
    public function initConfig($config)
    {
        $this->swoole_config = $config['swoole'];
        $this->redis_config = $config['redis'];
        $this->time_rate = $config['system']['time_rate'];
        $this->worker_num = $config['swoole']['worker_num'];
        $this->log_config = $config['log'];
    }

    //监听连接进入事件
    public function onConnect($serv, $fd)
    {
        var_dump($fd, $serv->stats());
        echo "Client: Connect.\n";
    }

    //监听数据接收事件
    public function onReceive($serv, $fd, $from_id, $data)
    {
        $msg = "Server: $fd ;data: " . $data;

        $serv->send($fd, $msg);
    }

    //woke 进程中循环
    public function onWorkerStart($serv, $workerId)
    {
        RedisModule::connect($this->redis_config);
        Log::add('Worker start : ' . $workerId);
        $this->serv = $serv;
        if ($workerId < $this->worker_num) {
            swoole_timer_tick($this->time_rate, function () {
                $this->onSendMsg($this->serv);
            });
        }
    }

    /**
     * @param $serv
     * @return bool
     * 转发消息
     */
    public function onSendMsg($serv)
    {
        if (!RedisModule::info()) {
            RedisModule::reload($this->redis_config);
        }
        $msgData = RedisModule::lpop(MAIN_MSG_KEY);
        if (empty($msgData)) return false;
        $serv->task($msgData);
    }

    //监听连接关闭事件
    public function onClose($serv, $fd)
    {
        echo "Client: $fd Close.\n";
    }

    public function onTask($serv, $fd, $fromId, $jsonData)
    {
        Log::add("onTask serv : $serv fd : $fd from_id : $fromId data : $jsonData");
        if (empty($jsonData)) {
            return array(
                'error_code' => 100,
                'data' => $jsonData
            );
        }

        return SendMsg::Handle($jsonData)
            ? array(
                'error_code' => 200,
                'data' => $jsonData
            )
            : array(
                'error_code' => 300,
                'data' => $jsonData
            );
    }

    /**
     * @param $data
     * 添加副队列 (相对配置)
     */
    public function addViceQueue($data)
    {
        $curLen = RedisModule::rpush(VICE_MSG_KEY, $data);
        if (0 < $curLen) {
            Log::add('addViceQueue data :' . $data);
        } else {
            Log::error('addViceQueue error data :' . $data);
        }
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $data
     * @return bool
     * 监控task 完成事件
     */
    public function onFinish($serv, $taskId, $data)
    {
        if (200 == $data['error_code']) {
            Log::add("onFinish Success code : {$data['error_code']} data : {$data['data']}");
            return true;
        }

        Log::error("onFinish Error code : {$data['error_code']} data : {$data['data']}");
        $this->addViceQueue($data['data']);

        return false;
    }

}


