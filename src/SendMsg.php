<?php

/**
 * Created by PhpStorm.
 * Author: Len
 * mail: i@91coder.org
 * Created Time: Sun 23 Apr 2017 02:27:34 AM CST
 *
 */
class SendMsg
{

    public static function Handle($jsonData)
    {
        $reqData = json_decode($jsonData, true);
        $uri = $reqData['uri'];
        $body = $reqData['body'];
        $method = $reqData['method'];

        switch (strtoupper($method)) {
            case 'GET':
                $result = self::get_curl($uri);
                break;
            default:
                $result = self::post_curl($uri, $body);
                break;
        }

        return $result;
    }

    private static function get_curl($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($curl);
        curl_close($curl);
        if (!$response) {
            return false;
        }
        $reqArr = json_decode($response, true);

        return !empty($reqArr['code']) && 200 == $reqArr['code'];
    }

    private static function post_curl($uri, $body)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $uri);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($body));
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        $response = curl_exec($curl);
        curl_close($curl);
        if (!$response) {
            return false;
        }
        $reqArr = json_decode($response, true);

        return !empty($reqArr['code']) && 200 == $reqArr['code'];
    }

}