<?php
/*
* File Name: autoload.php
* Author: Len
* mail: i@91coder.org
* Created Time: Sun 26 Mar 2017 09:57:52 PM CST
* 
*/
if (!function_exists('classAutoLoader')) {

    function classAutoLoader($class)
    {

        $classFile = __DIR__ . '/' . $class . '.php';

        if (is_file($classFile) && !class_exists($class)) {
            require_once($classFile);
        }
    }
}

spl_autoload_register('classAutoLoader');
